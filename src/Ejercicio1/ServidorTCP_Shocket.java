package Ejercicio1;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Semaphore;

public class ServidorTCP_Shocket {
    public static void main(String[] args) {
        int tamanoSemaforin = 2;
        Semaphore semaforo = new Semaphore(tamanoSemaforin);

        try {
            ServerSocket servidor = new ServerSocket(55000);
            System.out.println("Servidor escuchando en el puerto 50005...");

            for (int i = 0; i < 3; i++) {
                Socket cliente = servidor.accept();
                semaforo.acquire(); // Adquirir un permiso del semáforo
                System.out.println("Conexión establecida con " + cliente.getInetAddress());
                Thread clientThread = new Thread(new ClientHandler(cliente, semaforo));
                clientThread.setName("Hilo-" + (i+1));
                clientThread.start();
            }

        } catch (IOException e) {
            System.err.println("Error al crear el socket");
            System.out.println(e.getMessage());
        } catch (InterruptedException e) {
            System.err.println("Error al adquirir el permiso del semáforo");
            System.out.println(e.getMessage());
        }
    }

    private static class ClientHandler implements Runnable {
        private final Socket cliente;
        private final Semaphore semaforo;

        public ClientHandler(Socket cliente, Semaphore semaforo) {
            this.cliente = cliente;
            this.semaforo = semaforo;
        }

        @Override
        public void run() {
            try {
                DataInputStream entrada = new DataInputStream(cliente.getInputStream());
                DataOutputStream salida = new DataOutputStream(cliente.getOutputStream());

                double num1;
                double num2;
                String operador;

                //System.out.println(Thread.currentThread().getName() + ": Recibiendo mensaje:");
                System.out.println( ": Esperando mensaje de: " + Thread.currentThread().getName());
                try {
                    num1 = Double.parseDouble(entrada.readUTF());
                    System.out.println(Thread.currentThread().getName() + ": Numero 1 recibido " + num1);
                    salida.writeUTF("Primer numero recibido, ole");

                    System.out.println( ": Esperando mensaje de: " + Thread.currentThread().getName());
                    num2 = Double.parseDouble(entrada.readUTF());
                    System.out.println(Thread.currentThread().getName() + ": Numero 2 recibido " + num2);
                    salida.writeUTF("Segundo numero recibido, ole ole");

                    System.out.println( ": Esperando mensaje de: " + Thread.currentThread().getName());
                    operador = entrada.readUTF();
                    System.out.println(Thread.currentThread().getName() + ": Enviando mensaje al cliente:");
                    salida.writeUTF(calculadora(num1, num2, operador));
                    System.out.println(Thread.currentThread().getName() + ": Cerrando Streams y sockets...");
                } catch (EOFException e) {
                    System.out.println(Thread.currentThread().getName() + ": Cliente cerró la conexión abruptamente");
                }
                entrada.close();
                salida.close();
                cliente.close();
                semaforo.release(); // Liberar un permiso del semáforo
            } catch (IOException e) {
                System.err.println("Error al manejar la conexión con el cliente");
                System.out.println(e.getMessage());
            }
        }
        static String calculadora(double num1, double num2, String operador){
            String respuesta;
            switch (operador){
                case "+":
                    respuesta= ("la suma es: " + (num1+num2));
                    break;
                case "-":
                    respuesta= ("la resta es: " + (num1-num2));
                    break;
                case "*":
                    respuesta= ("la multiplicacion es: " + (num1*num2));
                    break;
                case "/":
                    respuesta= ("la division es: " + (num1/num2));
                    break;
                default:
                    respuesta= ("Esa operación no es válida");
                    break;
            }
            return respuesta;
        }
    }
}
