package Ejercicio2;

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Scanner;

public class ClienteUDP {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Creando y conectando el socket datagram cliente");
        try {
            DatagramSocket socket = new DatagramSocket();
            InetAddress serverAddress = InetAddress.getByName("localhost");
            int serverPort = 55000;

            for (int i = 0; i < 3; i++) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                DataOutputStream dos = new DataOutputStream(baos);

                switch (i) {
                    case 0:
                        System.out.println("(Escribe salir para finalizar antes)");
                        System.out.println("Escribe el primer numero:");
                        break;
                    case 1:
                        System.out.println("Escribe el segundo numero:");
                        break;
                    case 2:
                        System.out.println("Escribe el operador, ej: + , - , * , /");
                        break;
                }
                String mensaje = sc.nextLine();
                if (mensaje.equals("salir")) {
                    break;
                }
                dos.writeInt(mensaje.length());
                dos.writeBytes(mensaje);

                byte[] data = baos.toByteArray();
                DatagramPacket packet = new DatagramPacket(data, data.length, serverAddress, serverPort);
                socket.send(packet);
                System.out.println("Mensaje enviado");

                byte[] buffer = new byte[1024];
                DatagramPacket responsePacket = new DatagramPacket(buffer, buffer.length);
                socket.receive(responsePacket);

                String response = new String(responsePacket.getData(), 0, responsePacket.getLength());
                System.out.println("Respuesta del servidor: " + response);
            }
            System.out.println("Cerrando conexión con el servidor...");
            socket.close();
            System.out.println("Terminando");

        } catch (IOException e) {
            System.err.println("Error al crear el socket.");
            System.out.println(e.getMessage());
        }
    }
}
