package Ejercicio2;

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.concurrent.Semaphore;

public class ServidorUDP {
    public static void main(String[] args) {
        final int MAX_CONEXIONES = 2;
        Semaphore semaforo = new Semaphore(MAX_CONEXIONES);

        try {
            DatagramSocket socket = new DatagramSocket(55000);
            System.out.println("Servidor escuchando en el puerto 55000...");

            while (true) {
                byte[] buffer = new byte[1024];
                DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
                socket.receive(packet);
                semaforo.acquire(); // Adquirir un permiso del semáforo
                System.out.println("Conexión establecida con " + packet.getAddress());
                Thread clientThread = new Thread(new ClientHandler(socket, packet, semaforo));
                clientThread.start();
            }

        } catch (IOException | InterruptedException e) {
            System.err.println("Error en el servidor");
            System.out.println(e.getMessage());
        }
    }

    private static class ClientHandler implements Runnable {
        private final DatagramSocket socket;
        private final DatagramPacket packet;
        private final Semaphore semaphore;

        public ClientHandler(DatagramSocket socket, DatagramPacket packet, Semaphore semaphore) {
            this.socket = socket;
            this.packet = packet;
            this.semaphore = semaphore;
        }

        @Override
        public void run() {
            try {
                InetAddress clientAddress = packet.getAddress();
                int clientPort = packet.getPort();

                byte[] data = packet.getData();
                ByteArrayInputStream bais = new ByteArrayInputStream(data);
                DataInputStream dis = new DataInputStream(bais);

                int messageLength = dis.readInt(); // Leer la longitud de los datos
                byte[] messageBytes = new byte[messageLength];
                dis.readFully(messageBytes); // Leer los datos de acuerdo a la longitud recibida

                String operacion = new String(messageBytes);
                System.out.println("Operacion recibida: " + operacion);

                // Asumiendo que la operación es válida y enviando una respuesta simple
                String respuesta = "Operación recibida: " + operacion;
                byte[] responseBytes = respuesta.getBytes();

                DatagramPacket responsePacket = new DatagramPacket(responseBytes, responseBytes.length, clientAddress, clientPort);
                socket.send(responsePacket);

                semaphore.release(); // Liberar un permiso del semáforo
            } catch (IOException e) {
                System.err.println("Error al manejar la conexión con el cliente");
                System.out.println(e.getMessage());
            }
        }
    }
}
