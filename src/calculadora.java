import java.util.Scanner;

public class calculadora {
    public static void main(String[] args) {
        /*Añade al ejercicio 1 la pregunta de qué tipo de operación quiere que se realice, de tal
        forma que el usuario inserte dos números y la operación que quiere que se ejecute, y
        el programa devuelva la salida correcta.*/
        Scanner scl = new Scanner(System.in); //Scanner para cadenas de caracteres
        Scanner scn = new Scanner(System.in); //Scanner para números
        System.out.println("Inserta dos numeros y una operacion (sumar, restar, multiplicar, dividir)");
        double num1 = scn.nextDouble();
        double num2 = scn.nextDouble();
        String operacion = scl.nextLine();

        /*
        if (operacion.equals("sumar")){
            System.out.println("la suma es: " + (num1+num2));
        }else if(operacion.equals("restar")){
            System.out.println("la resta es: " + (num1-num2));
        }else if(operacion.equals("multiplicar")){
            System.out.println("la multiplicacion es: " + (num1*num2));
        }else if(operacion.equals("dividir")){
            System.out.println("la division es: " + (num1/num2));
        }else{
            System.out.println("Esa operación no es válida");
        }

        */
        switch (operacion){
            case "sumar":
                System.out.println("la suma es: " + (num1+num2));
                break;
            case "restar":
                System.out.println("la resta es: " + (num1-num2));
                break;
            case "multiplicar":
                System.out.println("la multiplicacion es: " + (num1*num2));
                break;
            case "dividir":
                System.out.println("la division es: " + (num1/num2));
                break;
            default:
                System.out.println("Esa operación no es válida");
                break;
        }

    }
}